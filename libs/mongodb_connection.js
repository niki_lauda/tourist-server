
var mongoose = require('mongoose');
var config = require('.././config');

//CONNECT TO MongoDB
mongoose.connect(config.mongoDB.mongodbUri);

var mongodb = mongoose.connection;

module.exports = mongodb;
