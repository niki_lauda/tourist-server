var fs = require('fs');
var inputFile = 'places.json';

module.exports = function(callback){
  var data = JSON.parse(fs.readFileSync(inputFile, 'utf8'));

  data.forEach(function(item){
    delete item.id;
    delete item.comments;
    delete item.addTime;
    delete item.shortDescription;
  });

  callback(data);
};
