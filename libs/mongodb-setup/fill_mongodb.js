var mongoose = require('mongoose');
var config = require('../../config');
var Place = require('../../models/place');
var parser = require('./dataparser');

mongoose.connect(config.mongoDB.mongodbUri);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {

  parser(function(data){
    var savesPending = data.length;

    var saveFinished = function(err, place){
      if(err){
        return console.error(err);
      }
      savesPending--;
      if(savesPending === 0){
        mongoose.disconnect();
      }
    };

    for(var i = 0; i < data.length ; i++){
      var place = new Place(data[i]);
      place.save(saveFinished);
    }
  });
});
