//IMPORT MODULES
var express = require('express');
var handlebars = require('express3-handlebars').create({defaultLayout:'main'});
var fs = require('fs');

var configPath = __dirname + '/config.json';
var uri = JSON.parse(fs.readFileSync(configPath));

module.exports = {

  mongoDB: uri,

  serverConfig: function(app){
    app.engine('handlebars', handlebars.engine);

    app.set('view engine', 'handlebars');
    app.set('port', process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3002);
    app.set('ip', process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1");

    //enabling access to static files
    app.use(express.static(__dirname + '/public'));

    //enabling access to bootstrap
    app.use('/scripts', express.static(__dirname + '/node_modules/bootstrap/dist/'));

  }
};
