var Place = require('../models/place');

module.exports = {
  registerRoutes: function(app){
    app.get('/api', this.api);
    app.get('/api/place', this.placeAll);
    app.get('/api/place/:id', this.placeById);
    app.put('/api/place/:id', this.placeUpdate);
    app.delete('/api/place/:id', this.placeDelete);
    app.post('/api/place', this.placeCreate);
  },

  api: function(req, res, next){
    res.render('api', {'title':'Tourist places RESTful API'});
  },

  placeAll: function(req, res, next){
      Place.find(function(err, places){
      res.type('json');
      if(err){
        res.send(JSON.stringify({"status" : "failed"}, null, 3));
        return;
      }
      res.send(JSON.stringify(places, null, 3));
    });
  },

  placeById: function(req, res, next){
    Place.findById(req.params.id, function(err, place){
      res.type('json');
      if(err){
        res.send(JSON.stringify({"status" : "failed"}, null, 3));
        return;
      }
      res.send(JSON.stringify(place, null, 3));
    });
  },

  placeUpdate: function(req, res, next){
    Place.findById(req.params.id, function(err, place){
      res.type('json');
      if(err){
        res.send(JSON.stringify({"status" : "failed"}, null, 3));
        return;
      }

      for(var attribute in req.body){
        if(req.body[attribute] !== undefined && place[attribute] !== undefined){
          place[attribute] = req.body[attribute];
          place.save(function(err){
            if(err){
              res.send(JSON.stringify({"status" : "failed"}, null, 3));
              return;
            }
          });
        }
      }
      res.send(JSON.stringify({"status" : "success"}, null, 3));
    });
  },

  placeDelete: function(req, res, next){
    Place.remove({_id:req.params.id}, function(err){
      res.type('json');
      if(err){
        res.send(JSON.stringify({"status" : "failed"}, null, 3));
        return;
      }
      res.send(JSON.stringify({"status" : "success"}, null, 3));
    });
  },

  placeCreate: function(req, res, next){
    var place = new Place(req.body);

    place.save(function(err){
      res.type('json');
      if(err){
        res.send(JSON.stringify({"status" : "failed"}, null, 3));
        return;
      }

      res.send(JSON.stringify({"status" : "success"}, null, 3));
    });
  }
};
