var mongoose = require('mongoose');

var placeSchema = mongoose.Schema({
  name: String,
  englishName: String,
  description: String,
  fotoName: String,
  address: String,
  telephoneNumber: String,
  webPage: String,
  email: String,
  country: String,
  city: String,
  region: String,
  coordX: Number,
  coordY: Number
});

var Place = mongoose.model('place', placeSchema);
module.exports = Place;
