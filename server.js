//node_modules
var express = require('express');
var handlebars = require('express3-handlebars').create({
  defaultLayout: 'main'
});
var mongoose = require('mongoose');
var config = require('./config');
var bodyParser = require('body-parser');

var apiController = require('./controllers/api');
var conn = require('./libs/mongodb_connection');
var Place = require('./models/place');

var TouristApp = function() {
  var self = this;

  /*  ================================================================  */
  /*  Helper functions.                                                 */
  /*  ================================================================  */

  /**
   *  Set up server IP address and port # using env variables/defaults.
   */
  self.setupVariables = function() {
    //  Set the environment variables we need.
    self.ipaddress = process.env.OPENSHIFT_NODEJS_IP;
    self.port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

    if (typeof self.ipaddress === "undefined") {
      //  Log errors on OpenShift but continue w/ 127.0.0.1 - this
      //  allows us to run/test the app locally.
      console.warn('No OPENSHIFT_NODEJS_IP var, using 127.0.0.1');
      self.ipaddress = "127.0.0.1";
    }
  };

  /**
   *  terminator === the termination handler
   *  Terminate server on receipt of the specified signal.
   *  @param {string} sig  Signal to terminate on.
   */
  self.terminator = function(sig) {
    if (typeof sig === "string") {
      console.log('%s: Received %s - terminating sample app ...',
        Date(Date.now()), sig);
      mongoose.disconnect();
      process.exit(1);
    }
    console.log('%s: Node server stopped.', Date(Date.now()));
  };

  /**
   *  Setup termination handlers (for exit and a list of signals).
   */
  self.setupTerminationHandlers = function() {
    //  Process on exit and signals.
    process.on('exit', function() {
      self.terminator();
    });

    ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
      'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
    ].forEach(function(element, index, array) {
      process.on(element, function() {
        self.terminator(element);
      });
    });
  };

  /*  ================================================================  */
  /*  App server functions (main app logic here).                       */
  /*  ================================================================  */

  /**
   *  Initialize the server (express) and create the routes and register
   *  the handlers.
   */

  self.createRoutes = function() {
    apiController.registerRoutes(self.app);
    self.app.get('/api/placeimg/:id', function(req, res, next){
      Place.findById(req.params.id, function(err, place){
        res.type('image/jpeg');
        if(err){
          res.send(JSON.stringify({"status" : "failed"}, null, 3));
          return;
        }
        res.sendFile(__dirname + '/web/images/' + place.fotoName);
      });

    });
  };

  self.initializeServer = function() {
    self.app = express();
    self.app.use(bodyParser.urlencoded({
      extended: false
    }));
    self.app.use(bodyParser.json());
    self.app.engine('handlebars', handlebars.engine);
    self.app.set('view engine', 'handlebars');
    //enabling access to static files
    //enabling access to bootstrap
    self.app.use('/scripts', express.static(__dirname + '/node_modules/bootstrap/dist/'));
    self.app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
    self.app.use('/libs', express.static(__dirname + '/node_modules/'));
    self.createRoutes();

    self.app.use(function(req, res) {
      res.render('404');
    });

  };

  /**
   *  Initializes the sample application.
   */
  self.initialize = function() {
    self.setupVariables();
    self.setupTerminationHandlers();

    // Create the express server and routes.
    self.initializeServer();
  };

  /**
   *  Start the server (starts up the sample application).
   */
  self.start = function() {
    //  Start the app on the specific interface (and port).
    self.app.listen(self.port, self.ipaddress, function() {
      console.log('%s: Node server started on %s:%d ...',
        Date(Date.now()), self.ipaddress, self.port);
    });
  };

};

var app = new TouristApp();
app.initialize();
app.start();
